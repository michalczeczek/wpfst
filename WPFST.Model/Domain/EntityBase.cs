﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WPFST.Model
{
    public abstract class EntityBase : INotifyPropertyChanged, IDataErrorInfo
    {
        [Index(IsUnique = true)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        [NotMapped]
        public string Error
        {
            get
            {
                return null;
            }
        }

        [NotMapped]
        public string this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        [NotMapped]
        protected abstract string[] ValidatedProperties { get; }

        [NotMapped]
        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {

                    if (GetValidationError(property) != null)
                        return false;
                }

                return true;
            }
        }

        protected abstract string GetValidationError(string propertyName);
    }
}
