﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFST.Model
{
    public class ProductOrder : EntityBase
    {
        private int quantity = 0;
        public int Quantity
        {
            get { return quantity; }
            set
            {
                if (value != quantity)
                {
                    quantity = value;
                    OnPropertyChanged("Quantity");
                    OnPropertyChanged("TotalPrice");
                }
            }
        }

        public int? ProductId { get; set; }

        private Product product;
        public virtual Product Product
        {
            get { return product; }
            set
            {
                if (value != product)
                {
                    product = value;
                    OnPropertyChanged("Product");
                }
            }
        }


        public int? OrderId { get; set; }

        private Order order;
        public virtual Order Order { get; set; }

        [NotMapped]
        public decimal TotalPrice
        {
            get
            {
                if (Product != null)
                    return Quantity * Product.Price;
                else
                    return 0;
            }
        }

        protected override string[] ValidatedProperties
        {
            get
            {
                return new String[] { "Quantity" };
            }
        }

        protected override string GetValidationError(string propertyName)
        {
            string error = null;

            if (propertyName == "Quantity")
                if (Quantity <= 0)
                    error = Properties.Resources.QuantityError;

            return error;
        }
    }
}
