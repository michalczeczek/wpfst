﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WPFST.Model
{
    public class Order : EntityBase
    {
        [StringLength(20)]
        [Index(IsUnique = true)]
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string name;

        public DateTime OrderDate { get; set; }

        public int? CustomerId { get; set;  }

        private Customer customer;
        public virtual Customer Customer
        {
            get { return customer; }
            set
            {
                if (value != customer)
                {
                    customer = value;
                    OnPropertyChanged("Customer");
                }
            }
        }

        [NotMapped]
        public decimal TotalPrice
        {
            get
            {
                if (Products.Count > 0)
                    return Products.Select(x => x.TotalPrice).Sum();
                else
                    return 0;
            }
        }

        public virtual ICollection<ProductOrder> Products
        {
            get { return products ?? (products = new Collection<ProductOrder>()); }
            set { products = value; OnPropertyChanged("Products"); }
        }
        private ICollection<ProductOrder> products;

        protected override string[] ValidatedProperties
        {
            get
            {
                return new String[] { "Name", "Customer", "Products" };
            }
        }

        protected override string GetValidationError(string propertyName)
        {
            string error = null;

            if (propertyName == "Name")
                if (String.IsNullOrEmpty(Name))
                    error = "Name can't be empty";

            if (propertyName == "Customer")
                if (Customer == null)
                    error = "Cutomer must exists";

            if (propertyName == "Products")
                if (Products.Count == 0)
                    error = "Order should contains at least one product";

            return error;
        }
    }
}
