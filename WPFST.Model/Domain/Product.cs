﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace WPFST.Model
{
    public enum Unit { Kg, l, m, Pcs }

    public class Product : EntityBase
    {
        [StringLength(20)]
        [Index(IsUnique = true)]
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string name;

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set
            {
                if (value != price)
                {
                    price = value;
                    OnPropertyChanged("Price");
                }
            }
        }

        private Unit unit;
        public Unit Unit
        {
            get { return unit; }
            set
            {
                if (value != unit)
                {
                    unit = value;
                    OnPropertyChanged("Unit");
                }
            }
        }

        protected override string[] ValidatedProperties
        {
            get
            {
                return new String[] { "Name", "Price" };
            }
        }

        protected override string GetValidationError(string propertyName)
        {
            string error = null;

            if (propertyName == "Name")
                if (String.IsNullOrEmpty(Name))
                    error = "Name can't be empty";

            if (propertyName == "Price")
                if (Price <= 0)
                    error = "Price must be greater than 0";

            return error;
        }

        public override string ToString()
        {
            if(Name != null)
            {
                return Name;
            }
            else
            {
                return base.ToString();
            }
        }
    }
}
