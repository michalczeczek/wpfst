﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace WPFST.Model
{
    public class Customer : EntityBase, IDataErrorInfo
    {
        [StringLength(20)]
        [Index(IsUnique = true)]
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        private string name;

        [NotMapped]
        protected override string[] ValidatedProperties
        {
            get
            {
               return new String[] { "Name" };
            }
        }

        protected override string GetValidationError(string propertyName)
        {
            string error = null;

            if (String.IsNullOrEmpty(Name))
                error = "Name can't be empty";
            
            return error;
        }
    }

}
