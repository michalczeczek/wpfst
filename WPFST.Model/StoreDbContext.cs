﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.Composition;
using System.Data.Entity.Core.Objects;

namespace WPFST.Model
{
    public class StoreDbInitializer : CreateDatabaseIfNotExists<StoreDbContext>
    {
        protected override void Seed(StoreDbContext context)
        {
            base.Seed(context);

            context.Customers.Add(new Customer() { Name = "Customer1" });
            context.Customers.Add(new Customer() { Name = "Customer2" });
            context.Customers.Add(new Customer() { Name = "Customer3" });
            context.Customers.Add(new Customer() { Name = "Customer4" });

            context.Products.Add(new Product() { Name = "Product1", Price = 1, Unit = Unit.Kg });
            context.Products.Add(new Product() { Name = "Product2", Price = 2, Unit = Unit.l });
            context.Products.Add(new Product() { Name = "Product3", Price = 3, Unit = Unit.m });
            context.Products.Add(new Product() { Name = "Product4", Price = 1, Unit = Unit.Pcs });

            context.Orders.Add(new Order() { CustomerId = 1, Name = "Order1", OrderDate = DateTime.Now });
            context.ProductOrders.Add(new ProductOrder() { OrderId = 1, ProductId = 1, Quantity = 2 });
        }
    }


    public class StoreDbContext : DbContext
    {
        public StoreDbContext() : base(Settings.ConnectionString)
        {
            Database.SetInitializer<StoreDbContext>(new StoreDbInitializer());
        }

        public IDbSet<Customer> Customers { get; set; }
        public IDbSet<Order> Orders { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<ProductOrder> ProductOrders { get; set; }
    }
}
