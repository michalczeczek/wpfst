﻿using System;
using System.Linq;
using System.Data.Entity;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;


namespace WPFST.Model
{
    public class Repository<TEntity> where TEntity : class
    {

        private IDbSet<TEntity> dbSet;
        private StoreDbContext dbContext;

        public Repository(StoreDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = this.dbContext.SetEntity<TEntity>();
        }

        public IQueryable<TEntity> GetAll(string include1 = null, string include2 = null)
        {
            if (include1 == null && include2 == null)
                return dbSet;
            else if (include2 == null)
                return dbSet.Include(include1);
            else
                return dbSet.Include(include1).Include(include2);
        }



        public TEntity Get(int id)
        {
            return dbSet.Find(id);
        }

        public IQueryable<TEntity> FindAll(Func<TEntity, bool> match)
        {
            return from set in dbSet where match(set) select set;
        }

        public TEntity Find(Func<TEntity, bool> match)
        {
            return dbSet.SingleOrDefault(match);
        }

        public void Add(TEntity record)
        {
            dbSet.Add(record);
        }

        public void Update(TEntity update, int key)
        {
            if (update == null)
                return;

            TEntity existing = dbSet.Find(key);
            if (existing != null)
                dbContext.Update<TEntity>(existing, update);
        }

        public void Delete(TEntity delete)
        {
            dbSet.Remove(delete);
        }

        public int Count()
        {
            return dbSet.Count();
        }

    }
}
