﻿using System;
using System.ComponentModel.Composition;


namespace WPFST.Model
{
    public class UnitOfWork : IDisposable
    {
        [Import]
        public StoreDbContext dbContext;

        public UnitOfWork()
        {

        }

        public Repository<TEntity> GetRepoInstance<TEntity>() where TEntity : class
        {
            return new Repository<TEntity>(dbContext);
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                    dbContext = null;
                }
            }
        }
    }
}
