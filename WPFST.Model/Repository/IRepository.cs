﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFST.Model
{
    public interface IRepository<TEntity> where TEntity: class
    {
        IQueryable<TEntity> GetAll();

        TEntity Get(int id);

        IQueryable<TEntity> FindAll(Func<TEntity, bool> match);

        TEntity Find(Func<TEntity, bool> match);

        void Add(TEntity record);

        void Update(TEntity update, int key);

        void Delete(TEntity delete);

        int Count();
    }
}
