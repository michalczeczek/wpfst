﻿using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.MefExtensions;
using System.Reflection;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace WPFST.Desktop
{
    class Bootstrapper : MefBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return ServiceLocator.Current.GetInstance<Shell>();
        }
       
        protected override void InitializeShell()
        { 
            base.InitializeShell();

            Application.Current.MainWindow = (Shell)this.Shell;
            Application.Current.MainWindow.Show();
        }

        protected override CompositionContainer CreateContainer()
        {
            var container =  base.CreateContainer();
            container.ComposeExportedValue(container);
            return container;
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            Container.SatisfyImportsOnce(typeof(ModuleManager));
        }

        protected override void ConfigureAggregateCatalog()
        {
            base.ConfigureAggregateCatalog();

            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Bootstrapper).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Orders.OrderModules).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Products.ProductModules).Assembly));
        }

    }
}
