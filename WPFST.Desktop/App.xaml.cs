﻿using System.Windows;
using System;
using System.Configuration;

namespace WPFST.Desktop
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            string cultrue =  ConfigurationManager.AppSettings["Culture"];

            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultrue);

            base.OnStartup(e);
            Bootstrapper bootstrapper = new Bootstrapper();

            bootstrapper.Run();
        }
    }
}
