﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using System.ComponentModel.Composition;
using Microsoft.Practices.ServiceLocation;

namespace WPFST.Products
{
    [ModuleExport(typeof(ProductModules))]
    public class ProductModules : IModule
    {

        [Import]
        public IRegionManager regionManager;

        public void Initialize()
        {
            this.regionManager.RegisterViewWithRegion("NavigationRegion", typeof(Views.ProductsNavigationView));
        }
    }
}
