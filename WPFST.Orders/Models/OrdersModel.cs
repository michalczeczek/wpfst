﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using WPFST.Model;
using System.Collections.ObjectModel;
using Microsoft.Practices.ServiceLocation;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;

namespace WPFST.Orders.Models
{
    [Export(typeof(IOrdersModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class OrdersModel : IOrdersModel
    {
        public OrdersModel()
        {
        }

        public List<Order> GetAllOrders()
        {
            var Orders = new List<Order>();
            using (var context = new StoreDbContext())
            {
                Orders = context.Set<Order>().Include(x => x.Customer).Include(x => x.Products.Select(y => y.Product)).ToList();
            }

            return Orders;
        }

        public void AddOrder(Order order)
        {
            using (var context = new StoreDbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var Order = new Order();
                        Order.Name = order.Name;
                        Order.CustomerId = order.Customer.ID;
                        Order.OrderDate = order.OrderDate;

                        foreach (var prod in order.Products)
                        {
                            Order.Products.Add(new ProductOrder() { ProductId = prod.Product.ID, Quantity = prod.Quantity });
                        }
                                        
                        context.Orders.Add(Order);
                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public void DeleteOrders(List<Order> ordersToDelete)
        {
            using (var context = new StoreDbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var deleteOrderIds = ordersToDelete.Select(x => x.ID);

                        var ProductsToDelete = context.ProductOrders.Where(x => deleteOrderIds.Contains(x.OrderId.Value));
                        var OrdersToDelete = context.Orders.Where(x => deleteOrderIds.Contains(x.ID));

                        context.Set<ProductOrder>().RemoveRange(ProductsToDelete);
                        context.Set<Order>().RemoveRange(OrdersToDelete);

                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public void EditOrder(Order order)
        {
            using (var context = new StoreDbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var EditOrder = new Order();
                        EditOrder.ID = order.ID;
                        EditOrder.Name = order.Name;
                        EditOrder.CustomerId = order.Customer.ID;
                        EditOrder.OrderDate = order.OrderDate;

                        foreach (var prod in order.Products)
                        {
                            if (prod.ID > 0)
                                context.Entry(prod).State = EntityState.Modified;
                            else
                                context.ProductOrders.Add(new ProductOrder() { OrderId = order.ID, ProductId = prod.Product.ID, Quantity = prod.Quantity });
                        }

                        context.Orders.Attach(EditOrder);
                        context.Entry(EditOrder).State = EntityState.Modified;

                        var usedProductsIds = order.Products.Select(x => x.ID).Where(x => x > 0);
                        var unusedProducts = context.ProductOrders.Where(x => x.OrderId == order.ID && !usedProductsIds.Contains(x.ID));
                        context.Set<ProductOrder>().RemoveRange(unusedProducts);

                        context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public List<ProductOrder> GetOrderProducts(int orderId)
        {
            List<ProductOrder> Products;

            using (var context = new StoreDbContext())
            {
                Products = context.ProductOrders.Where(x => x.OrderId == orderId).Include(x => x.Product).ToList();
            }

            return Products;
        }

        public Order GetOrder(string Name)
        {
            Order order;

            using (var context = new StoreDbContext())
            {
                order = context.Orders.Include(x => x.Customer).Include(x => x.Products.Select(y => y.Product)).First(x => x.Name == Name);
            }

            return order;
        }
    }
}
