﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Model;

namespace WPFST.Orders.Models
{
    [Export(typeof(IProductListModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ProductListModel : IProductListModel
    {
        public ProductListModel()
        {
        }

        public List<Product> GetAllProducts()
        {
            var Products = new List<Product>();
            using (var context = new StoreDbContext())
            {
                Products = context.Set<Product>().ToList();
            }

            return Products;
        }
    }
}
