﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Model;


namespace WPFST.Orders.Models
{
    [Export(typeof(ICustomerListModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CustomerListModel : ICustomerListModel
    {
        public CustomerListModel()
        {
        }

        public List<Customer> GetAllCustomers()
        {
            var Customers = new List<Customer>();
            using (var context = new StoreDbContext())
            {
                Customers = context.Set<Customer>().ToList();
            }

            return Customers;
        }
    }
}