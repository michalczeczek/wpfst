﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Model;
using System.Collections.ObjectModel;

namespace WPFST.Orders.Models
{
    public interface IOrdersModel
    {
        List<Order> GetAllOrders();
        Order GetOrder(string Name);

        void DeleteOrders(List<Order> ordersToDelete);
        void AddOrder(Order order);
        void EditOrder(Order order);
        List<ProductOrder> GetOrderProducts(int orderId);

    }
}
