﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Collections.ObjectModel;
//using WPFST.Model;

//namespace WPFST.Orders.Models.Fake
//{
//    [Export(typeof(IOrderListModel))]
//    [Export(typeof(ICustomerListModel))]
//    [Export(typeof(IProductListModel))]
//    [PartCreationPolicy(CreationPolicy.Shared)]
//    public class FakeModel : IOrderListModel, ICustomerListModel, IProductListModel
//    {
//        private Collection<Product> Products = new Collection<Product>()
//        {
//            new Product() {ID = 1, Name = "Product1", Price = 1, Unit = Unit.Kg },
//            new Product() {ID = 2, Name = "Product2", Price = 2, Unit = Unit.Pcs },
//            new Product() {ID = 3, Name = "Product3", Price = 3, Unit = Unit.m },
//            new Product() {ID = 4, Name = "Product4", Price = 4, Unit = Unit.l }
//        };

//        private Collection<Customer> Customers = new Collection<Customer>()
//        {
//            new Customer() { ID = 1, Name = "Customer1" },
//            new Customer() { ID = 2, Name = "Customer2" },
//            new Customer() { ID = 3, Name = "Customer3" }
//        };

//        private Collection<Order> Orders;


//        public FakeModel()
//        {
//            var ProductOrders = new Collection<ProductOrder>()
//            {
//                new ProductOrder() {ID = 1, Product = Products[0], Quantity = 1 },
//                new ProductOrder() {ID = 2, Product = Products[1], Quantity = 2 },
//                new ProductOrder() {ID = 3, Product = Products[2], Quantity = 3 },
//                new ProductOrder() {ID = 4, Product = Products[3], Quantity = 4 },
//                new ProductOrder() {ID = 5, Product = Products[1], Quantity = 2 }
//            };

//            Orders = new Collection<Order>()
//            {
//                new Order() { ID = 1, Customer = Customers[0], Name = "Order1", OrderDate = DateTime.Now, Products = new Collection<ProductOrder> () { ProductOrders[0], ProductOrders[1] } },
//                new Order() { ID = 2, Customer = Customers[1], Name = "Order2", OrderDate = DateTime.Now, Products = new Collection<ProductOrder> () { ProductOrders[2] } },
//                new Order() { ID = 3, Customer = Customers[2], Name = "Order3", OrderDate = DateTime.Now, Products = new Collection<ProductOrder> () { ProductOrders[3], ProductOrders[4] } }
//            };

//            ProductOrders[0].Order = Orders[0];
//            ProductOrders[1].Order = Orders[0];
//            ProductOrders[2].Order = Orders[1];
//            ProductOrders[3].Order = Orders[2];
//            ProductOrders[4].Order = Orders[2];
//        }

//        public List<Order> GetAllOrders()
//        {
//            return Orders.ToList();
//        }

//        public bool TryDeleteOrders(List<Order> ordersToDelete)
//        {
//            var order = Orders.First(x => x.ID == ordersToDelete.First().ID);
//            Orders.Remove(order);

//            return true;
//        }

//        public bool TryAddOrder(Order order)
//        {
//            order.ID = Orders.OrderBy(x => x.ID).Last().ID + 1;
//            Orders.Add(order);

//            return true;
//        }

//        public bool TryEditOrder(Order order)
//        {
//            var o = Orders.First(x => x.ID == order.ID);
//            Orders.Remove(o);

//            Orders.Add(order);

//            return true;
//        }

//        public List<Customer> GetAllCustomers()
//        {
//            return Customers.ToList();
//        }

//        public List<Product> GetAllProducts()
//        {
//            return Products.ToList();
//        }
//    }
//}