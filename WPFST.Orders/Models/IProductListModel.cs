﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Model;

namespace WPFST.Orders.Models
{
    public interface IProductListModel
    {
        List<Product> GetAllProducts();
    }
}
