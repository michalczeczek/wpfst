﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFST.Orders.Views
{
    /// <summary>
    /// Interaction logic for OrdersNavigationView.xaml
    /// </summary>
    [Export]
    [ViewSortHint("01")]
    public partial class OrdersNavigationView : UserControl, IPartImportsSatisfiedNotification
    {
        private static Uri ContentViewUri = new Uri("/OrderListView", UriKind.Relative);
        private static Uri RibbonViewUri = new Uri("/RibbonOrdersView", UriKind.Relative);

        [Import]
        public IRegionManager regionManager;

        public OrdersNavigationView()
        {
            InitializeComponent();
        }

        void IPartImportsSatisfiedNotification.OnImportsSatisfied()
        {
            IRegion mainContentRegion = this.regionManager.Regions["MainRegion"];
            if (mainContentRegion != null && mainContentRegion.NavigationService != null)
            {
                mainContentRegion.NavigationService.Navigated += this.MainContentRegion_Navigated;
            }
        }

        public void MainContentRegion_Navigated(object sender, RegionNavigationEventArgs e)
        {
            this.UpdateNavigationButtonState(e.Uri);
        }

        private void UpdateNavigationButtonState(Uri uri)
        {
            if (uri == ContentViewUri)
            {
                Button1.Background = Brushes.LightCoral;
            }
            else
            {
                Button1.Background = Brushes.WhiteSmoke;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.regionManager.RequestNavigate("RibbonRegion", RibbonViewUri);
            this.regionManager.RequestNavigate("MainRegion", ContentViewUri);
        }

    }
}
