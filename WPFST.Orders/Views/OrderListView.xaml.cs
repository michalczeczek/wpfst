﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using WPFST.Orders.ViewModels;

namespace WPFST.Orders.Views
{
    /// <summary>
    /// Interaction logic for OrderListView.xaml
    /// </summary>
    [Export("OrderListView")]
    public partial class OrderListView : UserControl
    {
        [ImportingConstructor]
        public OrderListView(OrderListViewModel viewModel)
        {       
            InitializeComponent();
            this.DataContext = viewModel;

            viewModel.RefreshOrders();
        }
    }
}
