﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFST.Orders.ViewModels;

namespace WPFST.Orders.Views
{
    /// <summary>
    /// Interaction logic for RibbonView.xaml
    /// </summary>
    [Export("RibbonOrdersView")]
    public partial class RibbonOrdersView : UserControl
    {

        [ImportingConstructor]
        public RibbonOrdersView(RibbonViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
} 

