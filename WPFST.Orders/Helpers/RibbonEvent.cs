﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFST.Orders.Helpers
{
    public enum RibbonAction { Refresh, AddNew, ShowAllDetails, HideAllDetails, ShowSelectedDetails}

    public class RibbonEvent : PubSubEvent<RibbonAction> { }
}
