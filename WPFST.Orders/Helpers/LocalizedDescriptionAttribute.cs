﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;

namespace WPFST.Orders.Helpers
{
    internal class LocalizedDescriptionAttribute : DescriptionAttribute
    {
        private readonly string resourceDescription;

        public LocalizedDescriptionAttribute(string description)
        : base()
    {
            this.resourceDescription = description;
        }

        public override string Description
        {
            get
            {
                return Properties.Resources.ResourceManager.GetString(resourceDescription);
            }
        }
    }
}
