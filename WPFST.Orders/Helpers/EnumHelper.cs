﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Controls;

namespace WPFST.Orders.Helpers
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum DetailsOption
    {
        [LocalizedDescription("ShowForSelected")]
        ShowSelected,

        [LocalizedDescription("ShowForAll")]
        ShowAll,

        [LocalizedDescription("Hide")]
        HideAll

    }

    public class EnumBindingSourceExtension : MarkupExtension
    {
        private Type _enumType;
        public Type EnumType
        {
            get { return this._enumType; }
            set
            {
                if (value != this._enumType)
                {
                    if (null != value)
                    {
                        Type enumType = Nullable.GetUnderlyingType(value) ?? value;
                        if (!enumType.IsEnum)
                            throw new ArgumentException("Type must be for an Enum.");
                    }

                    this._enumType = value;
                }
            }
        }

        public EnumBindingSourceExtension() { }

        public EnumBindingSourceExtension(Type enumType)
        {
            this.EnumType = enumType;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (null == this._enumType)
                throw new InvalidOperationException("The EnumType must be specified.");

            Type actualEnumType = Nullable.GetUnderlyingType(this._enumType) ?? this._enumType;
            Array enumValues = Enum.GetValues(actualEnumType);

            if (actualEnumType == this._enumType)
                return enumValues;

            Array tempArray = Array.CreateInstance(actualEnumType, enumValues.Length + 1);
            enumValues.CopyTo(tempArray, 1);
            return tempArray;
        }
    }


    public class EnumDescriptionTypeConverter : EnumConverter
    {
        public EnumDescriptionTypeConverter(Type type)
            : base(type)
        {
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                if (value != null)
                {
                    FieldInfo fi = value.GetType().GetField(value.ToString());
                    if (fi != null)
                    {
                        var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        return ((attributes.Length > 0) && (!String.IsNullOrEmpty(attributes[0].Description))) ? attributes[0].Description : value.ToString();
                    }
                }

                return string.Empty;
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class EnumDescriptionConverter : IValueConverter
    {
        public EnumDescriptionConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType == typeof(string))
            {
                if (value != null)
                {
                    FieldInfo fi = value.GetType().GetField(value.ToString());
                    if (fi != null)
                    {
                        var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                        return ((attributes.Length > 0) && (!String.IsNullOrEmpty(attributes[0].Description))) ? attributes[0].Description : value.ToString();
                    }
                }
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (DetailsOption)Enum.Parse(typeof(DetailsOption), value.ToString());
        }
    }

    public class OptionToVisibilityModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var option = (DetailsOption)value;
            var visibility = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;

            switch(option)
            {
                case DetailsOption.HideAll:
                    visibility = DataGridRowDetailsVisibilityMode.Collapsed;
                    break;

                case DetailsOption.ShowAll:
                    visibility = DataGridRowDetailsVisibilityMode.Visible;
                    break;

                case DetailsOption.ShowSelected:
                    visibility = DataGridRowDetailsVisibilityMode.VisibleWhenSelected;
                    break;
            }

            return visibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var visibility = (DataGridRowDetailsVisibilityMode)value;
            var option = DetailsOption.ShowSelected;

            switch(visibility)
            {
                case DataGridRowDetailsVisibilityMode.Collapsed:
                    option = DetailsOption.HideAll;
                    break;

                case DataGridRowDetailsVisibilityMode.Visible:
                    option = DetailsOption.ShowAll;
                    break;

                case DataGridRowDetailsVisibilityMode.VisibleWhenSelected:
                    option = DetailsOption.ShowSelected;
                    break;
            }

            return option;
        }
    }
}
