﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using System.ComponentModel.Composition;
using Microsoft.Practices.ServiceLocation;

namespace WPFST.Orders
{
    [ModuleExport(typeof(OrderModules))]
    public class OrderModules : IModule
    {
        private static Uri ContentViewUri = new Uri("/OrderListView", UriKind.Relative);
        private static Uri RibbonViewUri = new Uri("/RibbonOrdersView", UriKind.Relative);

        [Import]
        public IRegionManager regionManager;

        public void Initialize()
        {
            this.regionManager.RegisterViewWithRegion("NavigationRegion", typeof(Views.OrdersNavigationView));
            this.regionManager.RequestNavigate("RibbonRegion", RibbonViewUri);
            this.regionManager.RequestNavigate("MainRegion", ContentViewUri);
        }
    }
}
