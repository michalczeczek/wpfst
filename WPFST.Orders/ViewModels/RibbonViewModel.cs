﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.Windows.Input;
using Microsoft.Practices.Prism.Regions;
using WPFST.Orders.Helpers;
using Microsoft.Practices.Prism.PubSubEvents;

namespace WPFST.Orders.ViewModels
{
    [Export]
    public class RibbonViewModel : NotificableViewModel, IPartImportsSatisfiedNotification
    {
        
        private static Uri OrdersViewUri = new Uri("WPFST.Orders.Views.OrderListView", UriKind.Relative);

        [Import]
        IEventAggregator eventAggregator;

        public DetailsOption _orderDetailsOption;
        public DetailsOption OrderDetailsOption
        {
            get { return _orderDetailsOption;  }
            set
            {
                if (_orderDetailsOption != value)
                {
                    _orderDetailsOption = value;
                    SetDetailsOption(value);
                    NotifyPropertyChanged("OrderDetailsOption");
                }
            }
        }

        private ICommand _showAllCommand;
        public ICommand ShowAllCommand
        {
            get
            {
                return _showAllCommand ?? (_showAllCommand = new RelayCommand(x => true, x => this.ShowAll()));
            }
        }

        private ICommand _createNewCommand;
        public ICommand CreateNewCommand
        {
            get
            {
                return _createNewCommand ?? (_createNewCommand = new RelayCommand(x => true, x => this.CreateNew()));
            }
        }

        [Import]
        public IRegionManager regionManager;

        public RibbonViewModel()
        {
            
        }

        public void OnImportsSatisfied()
        {
            OrderDetailsOption = DetailsOption.ShowSelected;
            eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.Refresh);
        }

        private void ShowAll()
        {
            this.regionManager.RequestNavigate("MainRegion", OrdersViewUri);
            eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.Refresh);
            SetDetailsOption(OrderDetailsOption);
        }

        private void CreateNew()
        {
            eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.AddNew);
            SetDetailsOption(OrderDetailsOption);
        }

        private void SetDetailsOption(DetailsOption option)
        {
            switch (option)
            {
                case DetailsOption.HideAll:
                    eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.HideAllDetails);
                    break;

                case DetailsOption.ShowAll:
                    eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.ShowAllDetails);
                    break;

                case DetailsOption.ShowSelected:
                    eventAggregator.GetEvent<RibbonEvent>().Publish(RibbonAction.ShowSelectedDetails);
                    break;
            }
        }
    }

}
