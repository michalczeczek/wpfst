﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Orders.Models;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Collections.ObjectModel;
using WPFST.Model;
using WPFST.Orders.Helpers;
using System.Windows.Input;
using System.Windows;
using Microsoft.Practices.Prism.PubSubEvents;
using System.Collections;

namespace WPFST.Orders.ViewModels
{
    [Export]
    public class OrderListViewModel : NotificableViewModel, IPartImportsSatisfiedNotification
    {
        private IOrdersModel orderModel;

        [Import]
        private CompositionContainer container;

        [Import]
        IEventAggregator eventAggregator;

        public DetailsOption _detailsOption;
        public DetailsOption DetailsOption
        {
            get { return _detailsOption; }
            set
            {
                if (_detailsOption != value)
                {
                    _detailsOption = value;
                    NotifyPropertyChanged("DetailsOption");
                }
            }
        }

        private ObservableCollection<Order> _orders;
        public ObservableCollection<Order> Orders
        {
            get { return _orders; }
            set
            {
                if (_orders != value)
                {
                    _orders = value;
                    NotifyPropertyChanged("Orders");
                }
            }
        }
        private IList _selectedOrders = new ArrayList();
        public IList SelectedOrders
        {
            get { return _selectedOrders; }
            set
            {
                _selectedOrders = value;
                NotifyPropertyChanged("SelectedOrders");
            }
        }

        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand(x => true, x => this.DeleteOrder()));
            }
        }

        private ICommand _editCommand;
        public ICommand EditCommand
        {
            get
            {
                return _editCommand ?? (_editCommand = new RelayCommand(x => SelectedOrders.Count == 1, x => { ShowEditOrderWindow(SelectedOrders[0] as Order,EditOrderViewModel.OrderAction.Edit); } ));
            }
        }

        private ICommand _copyCommand;
        public ICommand CopyCommand
        {
            get
            {
                return _copyCommand ?? (_copyCommand = new RelayCommand(x => SelectedOrders.Count == 1, x => { ShowEditOrderWindow(SelectedOrders[0] as Order, EditOrderViewModel.OrderAction.Create); }));
            }
        }

        [ImportingConstructor]
        public OrderListViewModel(IOrdersModel model)
        {
            orderModel = model;
        }

        public void OnImportsSatisfied()
        {
            eventAggregator.GetEvent<RibbonEvent>().Subscribe(RibbonAction);
        }

        private void RibbonAction(RibbonAction ribbonAction)
        {
            switch (ribbonAction)
            {
                case Helpers.RibbonAction.Refresh:
                    RefreshOrders();
                    break;

                case Helpers.RibbonAction.AddNew:
                    ShowEditOrderWindow(new Order(), EditOrderViewModel.OrderAction.Create);
                    break;

                case Helpers.RibbonAction.HideAllDetails:
                case Helpers.RibbonAction.ShowAllDetails:
                case Helpers.RibbonAction.ShowSelectedDetails:
                    DetailsOption = ParseRibbonActionToDetailOption(ribbonAction);
                    break;
            }
        }

        public void RefreshOrders()
        {
            Orders = new ObservableCollection<Order>(orderModel.GetAllOrders());
        }

        private void DeleteOrder()
        {
            var ordersToDelete = SelectedOrders.Cast<Order>().ToList();

            var result = MessageBoxResult.Yes;
            if (ordersToDelete.Count > 1)
                result = MessageBox.Show("Delete all selected orders?", "Delete orders", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.No)
            {
                return;
            }

            try
            {
                orderModel.DeleteOrders(ordersToDelete);
                foreach (var order in ordersToDelete)
                    Orders.Remove(order);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                    ex = ex.InnerException;

                MessageBox.Show("Can't delete order(s). " + ex.Message);
            }
        }


        private void ShowEditOrderWindow(Order order, EditOrderViewModel.OrderAction orderAction)
        {
            var editViewModel = new EditOrderViewModel(order, orderAction);
            container.SatisfyImportsOnce(editViewModel);
            
            var editView = new Views.EditOrderView(editViewModel);
            bool? returnValue = editView.ShowDialog();

            if ((returnValue ?? false))
            {
                if (orderAction == EditOrderViewModel.OrderAction.Edit)
                    Orders.Remove(order);

                Orders.Add(editViewModel.Order);
            }
        }

        private DetailsOption ParseRibbonActionToDetailOption(RibbonAction action)
        {
            DetailsOption option;

            switch(action)
            {
                case Helpers.RibbonAction.ShowAllDetails:
                    option = DetailsOption.ShowAll;
                    break;

                case Helpers.RibbonAction.HideAllDetails:
                    option = DetailsOption.HideAll;
                    break;

                case Helpers.RibbonAction.ShowSelectedDetails:
                    option = DetailsOption.ShowSelected;
                    break;

                default:
                    throw new FormatException("Rbbin acction can't be converted");

            }

            return option;
        }
    }
}
