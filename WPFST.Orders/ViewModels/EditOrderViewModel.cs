﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFST.Model;
using WPFST.Orders.Helpers;
using System.ComponentModel.Composition;
using System.Collections.ObjectModel;
using WPFST.Orders.Models;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows;
using System.Text.RegularExpressions;

namespace WPFST.Orders.ViewModels
{
    [Export]
    public class EditOrderViewModel : NotificableViewModel, IDataErrorInfo, IPartImportsSatisfiedNotification
    {
        public enum OrderAction { Edit, Create}

        private OrderAction orderAction;

        public Action CloseAction { get; set; }

        [Import]
        private ICustomerListModel customerModel;
        [Import]
        private IProductListModel productModel;
        [Import]
        private IOrdersModel ordersModel;

        private Order _order;
        public Order Order
        {
            get { return _order; }
            set
            {
                if (_order != value)
                {
                    _order = value;
                    NotifyPropertyChanged("Order");
                }
            }
        }

        private readonly Regex OrderNameRegex = new Regex("^[a-zA-Z0-9]*$");
        private string _orderName;
        public string OrderName
        {
            get { return _orderName; }
            set
            {
                if (_orderName != value)
                {
                    _orderName = value;
                    NotifyPropertyChanged("OrderName");
                }
            }
        }

        public decimal TotalPrice
        {
            get
            {
                if (OrderProducts.Count > 0)
                    return OrderProducts.Select(x => x.TotalPrice).Sum();
                else
                    return 0;
            }
        }

        private ObservableCollection<Customer> _allCustomers;
        public ObservableCollection<Customer> AllCustomers
        {
            get { return _allCustomers; }
        }
        private Customer _selectedCustomer;
        public Customer SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                if (_selectedCustomer != value)
                {
                    _selectedCustomer = value;
                    NotifyPropertyChanged("SelectedCustomer");
                }
            }
        }

        private ObservableCollection<Product> _allProducts;
        public ObservableCollection<Product> AllProducts
        {
            get { return _allProducts; }
        }
        private Product _selectedProductToAdd;
        public Product SelectedProductToAdd
        {
            get { return _selectedProductToAdd; }
            set
            {
                if (_selectedProductToAdd != value)
                {
                    _selectedProductToAdd = value;
                    NotifyPropertyChanged("SelectedProductToAdd");
                    NotifyPropertyChanged("SelectedProductToolTip");
                }
            }
        }
        public string SelectedProductToolTip
        {
            get
            {
                if (SelectedProductToAdd != null)
                    return String.Format("{0}: {1}, {2}: {3}",Properties.Resources.Unit, SelectedProductToAdd.Unit, Properties.Resources.Price, SelectedProductToAdd.Price);
                else
                    return "";
            }
        }


        private int _productQuantity;
        public int ProductQuantity
        {
            get { return _productQuantity; }
            set
            {
                if (_productQuantity != value)
                {
                    _productQuantity = value;
                    NotifyPropertyChanged("ProductQuantity");
                }
            }
        }

        private ObservableCollection<ProductOrder> _orderProducts;
        public ObservableCollection<ProductOrder> OrderProducts
        {
            get { return _orderProducts; }
            set
            {
                if (_orderProducts != value)
                {
                    _orderProducts = value;
                    NotifyPropertyChanged("OrderProducts");
                }
            }
        }
        private ProductOrder _selectedOrderProduct;
        public ProductOrder SelectedOrderProduct
        {
            get { return _selectedOrderProduct; }
            set
            {
                if (_selectedOrderProduct != value)
                {
                    _selectedOrderProduct = value;
                    NotifyPropertyChanged("SelectedOrderProduct");
                }
            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return _saveCommand ?? (_saveCommand = new RelayCommand(x => this.CanSave, x => this.Save()));
            }
        }
        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get
            {
                return _addCommand ?? (_addCommand = new RelayCommand(x => this.CanAddProduct, x => this.AddOrderProduct()));
            }
        }
        private ICommand _deleteCommand;
        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ?? (_deleteCommand = new RelayCommand(x => true, x => this.DeleteOrderProduct()));
            }
        }

        private string _viewName;
        public string ViewName
        {
            get { return _viewName; }
            set
            {
                if (_viewName != value)
                {
                    _viewName = value;
                    NotifyPropertyChanged("ViewName");
                }
            }
        }

        public string Error
        {
            get
            {
                return null;
            }
        }

        public string this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }

        static readonly string[] ValidatedProperties =
        {
            "SelectedCustomer",
            "OrderProducts",
            "OrderName"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {

                    if (GetValidationError(property) != null)
                        return false;
                }

                return true;
            }
        }

        private string GetValidationError(string propertyName)
        {
            string error = null;

            switch (propertyName)
            {
                case "SelectedCustomer":
                    if (SelectedCustomer == null || SelectedCustomer.Name == Properties.Resources.Empty)
                        error = Properties.Resources.SelectedCustomerError;
                    break;

                case "OrderProducts":
                    if (OrderProducts.Count == 0)
                        error = Properties.Resources.OrderProductsCountError;
                    else if (OrderProducts.Any(x => x.IsValid == false))
                        error = Properties.Resources.OrderProductsAnyError;
                    break;

                case "OrderName":
                    if (String.IsNullOrEmpty(OrderName) || !OrderNameRegex.IsMatch(OrderName))
                        error = Properties.Resources.OrderNameError;
                    break;

                default:
                    error = null;
                    throw new Exception(Properties.Resources.UnexpectedPropertyError);
            }

            return error;
        }

        public EditOrderViewModel(Order order, OrderAction orderAction)
        {
            _order = order;
            this.orderAction = orderAction;        
        }

        public void OnImportsSatisfied()
        {
            OrderName = Order.Name;
            _allCustomers = new ObservableCollection<Customer>(customerModel.GetAllCustomers());
            _allCustomers.Add(new Customer() { Name = Properties.Resources.Empty });
            if (Order.CustomerId != null)
            {
                SelectedCustomer = _allCustomers.First(x => x.ID == Order.CustomerId);
            }
            else
            {
                SelectedCustomer = _allCustomers.First(x => x.Name == Properties.Resources.Empty);
            }

            _allProducts = new ObservableCollection<Product>(productModel.GetAllProducts());
            SelectedProductToAdd = _allProducts.FirstOrDefault();

            _orderProducts = new ObservableCollection<ProductOrder>(ordersModel.GetOrderProducts(Order.ID));

            var orderId = Order.ID;
            var orderDate = Order.OrderDate;
            Order = new Order() { ID = orderId, OrderDate = orderDate};

            ViewName = orderAction == OrderAction.Create ? Properties.Resources.NewOrder : Properties.Resources.EditOrder;
        }

        private void AddOrderProduct()
        {
            var existProduct = OrderProducts.FirstOrDefault(x => x.Product.ID == SelectedProductToAdd.ID);
            if (existProduct != null)
            {
                existProduct.Quantity += ProductQuantity;
            }
            else
            {
                OrderProducts.Add(new ProductOrder() { Product = SelectedProductToAdd, Quantity = ProductQuantity});
            }

            NotifyPropertyChanged("TotalPrice");
        }

        private bool CanAddProduct
        {
            get
            {
                return SelectedProductToAdd != null && ProductQuantity > 0;
            }
        }

        private void DeleteOrderProduct()
        {
            OrderProducts.Remove(SelectedOrderProduct);
            NotifyPropertyChanged("TotalPrice");
        }

        private void Save()
        {
            Order.Name = OrderName;
            Order.Products = OrderProducts;
            Order.Customer = SelectedCustomer;
            
            try
            {
                if (orderAction == OrderAction.Create)
                {
                    Order.OrderDate = DateTime.Now;
                    ordersModel.AddOrder(Order);
                }
                else
                    ordersModel.EditOrder(Order);

                Order = ordersModel.GetOrder(OrderName);

                CloseAction();
            }
            catch(Exception ex)
            {

                while (ex.InnerException != null)
                    ex = ex.InnerException;

                MessageBox.Show(Properties.Resources.CantSaveChanges + " " + ex.Message);
            }          
        }

        private bool CanSave
        {
            get
            {
                return this.IsValid;
            }
        }


    }
}

